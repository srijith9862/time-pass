import pygame
import math
import random
import time
import config
from config import white

# initialising the pygame
pygame.init()

# creating the screen
screen = pygame.display.set_mode((1910, 990))

pygame.display.set_caption("happy_games")

clock = pygame.time.Clock()

counter = 1
scorer1 = 0
scorer2 = 0
k = 1
a = 1
set1 = 0
set2 = 0
# player1
car_img = pygame.image.load('car.png')
car_x = 925
car_y = 990
# player2
car1_x = 925
car1_y = 20

# ships
boat_img = []
boat_x = []
boat_y = []
boat_x_change = []
boat_y_change = []
num_of_boats = 5

for i in range(num_of_boats):
    boat_img.append(pygame.image.load('boat.png'))
    boat_img[i] = pygame.transform.scale(boat_img[i], (100, 100))
    boat_x.append(random.randint(0, 1910))
    boat_x_change.append(4 + 2 * counter)
    boat_y.append(630 - (i - 1) * 150)

sea_img = []
sea_x = []
sea_y = []
sea_x_change = []
sea_y_change = []
num_of_seas = 5

for i in range(num_of_seas):
    sea_img.append(pygame.image.load('sea.png'))
    sea_x.append(random.randint(0, 1910))
    sea_x_change.append(4 + 2 * counter)
    sea_y.append(660 - (i - 1) * 150)

shark_img = []
shark_x = []
shark_y = []
shark_x_change = []
shark_y_change = []
num_of_sharks = 5

for i in range(num_of_sharks):
    shark_img.append(pygame.image.load('shark.png'))
    shark_x.append(random.randint(0, 1910))
    shark_x_change.append(4 + 2 * counter)
    shark_y.append(660 - (i - 1) * 150)

fire_img = []
fire_x = []
fire_y = []
fire_x_change = []
fire_y_change = []
num_of_fires = 5

for i in range(num_of_fires):
    fire_img.append(pygame.image.load('fire.png'))
    fire_x.append(random.randint(0, 1910))
    fire_y.append(580 - (i - 1) * 150)

fire1_x = []
fire1_y = []
fire1_x_change = []
fire1_y_change = []
for i in range(num_of_fires):
    fire_img.append(pygame.image.load('fire.png'))
    fire1_x.append(random.randint(0, 1910))
    fire1_y.append(580 - (i - 1) * 150)

fire2_x = []
fire2_y = []
fire2_x_change = []
fire2_y_change = []
for i in range(num_of_fires):
    fire_img.append(pygame.image.load('fire.png'))
    fire2_x.append(random.randint(0, 1910))
    fire2_y.append(580 - (i - 1) * 150)

score_value = 0
score1_value = 0
time1 = 0
time2 = 0
font = pygame.font.Font('freesansbold.ttf', 32)

over_font = pygame.font.Font('freesansbold.ttf', 100)


def show():
    score = font.render("Player1 Score : " + str(score_value), True, white)
    screen.blit(score, (20, 20))


def show2():
    score = font.render("Player2 Score : " + str(score1_value), True, white)
    screen.blit(score, (50, 20))


# initialising the player1
def car(x, y):
    screen.blit(car_img, (x, y))


# initialising the player2
def car1(x, y):
    screen.blit(car_img, (x, y))


# initialising the boats
def boat(x, y, a):
    screen.blit(boat_img[a], (x, y))


# initialising the bottle
def sea(x, y, a):
    screen.blit(sea_img[a], (x, y))


# initialising the shark
def shark(x, y, a):
    screen.blit(shark_img[a], (x, y))


# initialising the fire
def fire(x, y, a):
    screen.blit(fire_img[a], (x, y))


# initialising the fire1
def fire1(x, y, a):
    screen.blit(fire_img[a], (x, y))


# initialising the fire2
def fire2(x, y, a):
    screen.blit(fire_img[a], (x, y))


# when they crash with boat
def collided_boat(x, y, x1, y1):
    distance = math.sqrt(math.pow(x - x1 + 36, 2) + math.pow(y - y1 + 36, 2))

    if distance < 60:
        return True
    else:
        return False


# when they crash with bottle
def collided_sea(x, y, x1, y1):
    distance = math.sqrt(math.pow(x - x1, 2) + math.pow(y - y1, 2))

    if distance < 50:
        return True
    else:
        return False


# when they crash with shark
def collided_shark(x, y, x1, y1):
    distance = math.sqrt(math.pow(x - x1, 2) + math.pow(y - y1, 2))

    if distance < 45:
        return True
    else:
        return False


# when they crash with fire
def collided_fire(x, y, x1, y1):
    distance = math.sqrt(math.pow(x - x1, 2) + math.pow(y - y1, 2))

    if distance < 31:
        return True
    else:
        return False


# when they crash with fire1
def collided_fire1(x, y, x1, y1):
    distance = math.sqrt(math.pow(x - x1, 2) + math.pow(y - y1, 2))

    if distance < 31:
        return True
    else:
        return False


# when they crash with fire2
def collided_fire2(x, y, x1, y1):
    distance = math.sqrt(math.pow(x - x1, 2) + math.pow(y - y1, 2))

    if distance < 31:
        return True
    else:
        return False


# when the game is over
# def gameover():
#    t = 500
#    while t:
#        screen.fill((220, 20, 60))
#        text = over_font.render("GAME OVER", 1, (255, 255, 0))
#        screen.blit(text, (650, 450))
#        pygame.display.update()
#        t -= 1


def final1(points1, time3):
    score_val = font.render("Score of player1 :  " + str(points1) + str(time3), True, (255, 0, 0))
    screen.blit(score_val, (600, 100))


def final2(points2, time3):
    score_val = font.render("Score of player2 :  " + str(points2) + str(time3), True, (255, 0, 0))
    screen.blit(score_val, (600, 200))


def showit(score2):
    score = font.render("GAME OVER", True, white)
    screen.blit(score, (420, 300))
    score1 = font.render("Click ENTER to continue", True, white)
    screen.blit(score1, (420, 500))
    score_val = font.render("Score of player1 :  " + str(score2), True, white)
    screen.blit(score_val, (350, 540))


def showit2(score10):
    score = font.render("GAME OVER", True, white)
    screen.blit(score, (420, 300))
    score1 = font.render("Click ENTER to continue", True, white)
    screen.blit(score1, (420, 500))
    score_val = font.render("Score of player2 :  " + str(score10), True, white)
    screen.blit(score_val, (350, 580))


# game loop
screenon = True
second = 0
speed = 4
while screenon:
    if counter <= 5:
        if counter == 1:
            speed = 4
        elif counter == 2:
            speed = 6
        elif counter == 3:
            speed = 8
        elif counter == 4:
            speed = 10
        elif counter == 5:
            speed == 12

        if a == 1 and k % 2 == 1:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    screenon = False

            screen.fill((0, 255, 255))
            finish = font.render("Finish", True, (220, 20, 60))
            screen.blit(finish, (900, 20))
            start = font.render("Start", True, (220, 20, 60))
            screen.blit(start, (900, 960))

            pressed = pygame.key.get_pressed()
            if pressed[pygame.K_UP]:
                car_y -= speed
            if pressed[pygame.K_DOWN]:
                car_y += speed
            if pressed[pygame.K_LEFT]:
                car_x -= speed
            if pressed[pygame.K_RIGHT]:
                car_x += speed

            time_passed = clock.tick(30)
            time_passed_seconds = time_passed / 1000
            second += time_passed_seconds
            print(second)
            second1 = font.render("Time :" + str(int(second)), True, (255, 255, 255))
            screen.blit(second1, (1500, 20))

            pygame.draw.line(screen, (220, 20, 60), (0, 60), (1910, 60), 10)
            pygame.draw.line(screen, (220, 20, 60), (0, 920), (1910, 920), 10)
            pygame.draw.line(screen, (0, 0, 255), (0, 150), (1910, 150), 40)
            pygame.draw.line(screen, (0, 0, 255), (0, 300), (1910, 300), 40)
            pygame.draw.line(screen, (0, 0, 255), (0, 450), (1910, 450), 40)
            pygame.draw.line(screen, (0, 0, 255), (0, 600), (1910, 600), 40)
            pygame.draw.line(screen, (0, 0, 255), (0, 750), (1910, 750), 40)

            if car_x <= 0:
                car_x = 1
            elif car_x >= 1864:
                car_x = 1863
            elif car_y <= 1:
                car_y = 1
            elif car_y >= 925:
                car_y = 925

            # boats movement
            for i in range(num_of_boats):

                # game over
                collision = collided_boat(boat_x[i], boat_y[i], car_x, car_y)
                if collision:
                    pygame.mixer.music.load('crash.mp3')
                    pygame.mixer.music.play()
                    # gameover()
                    a = 0
                    set2 = 1
                    car_x = 925
                    car_y = 990
                    # screenon = 0
                    # break
                boat_x[i] += boat_x_change[i]
                if boat_x[i] <= 0:
                    boat_x_change[i] = 1
                elif boat_x[i] >= 1864:
                    boat_x[i] = 0
                boat(boat_x[i], boat_y[i], i)

            for i in range(num_of_seas):

                # game over
                collision = collided_sea(sea_x[i], sea_y[i], car_x, car_y)
                if collision:
                    pygame.mixer.music.load('glass.mp3')
                    pygame.mixer.music.play()
                    # gameover()
                    a = 0
                    set2 = 1
                    car_x = 925
                    car_y = 990
                    # screenon = 0
                    # break
                sea_x[i] += sea_x_change[i]
                if sea_x[i] <= 0:
                    sea_x_change[i] = 1
                elif sea_x[i] >= 1864:
                    sea_x[i] = 0
                sea(sea_x[i], sea_y[i], i)

            for i in range(num_of_sharks):

                # game over
                collision = collided_shark(shark_x[i], shark_y[i], car_x, car_y)
                if collision:
                    pygame.mixer.music.load('shark.mp3')
                    pygame.mixer.music.play()
                    # gameover()
                    a = 0
                    set2 = 1
                    car_x = 925
                    car_y = 990
                    # screenon = 0
                    # break
                shark_x[i] += shark_x_change[i]
                if shark_x[i] <= 0:
                    shark_x_change[i] = 1
                elif shark_x[i] >= 1864:
                    shark_x[i] = 0
                shark(shark_x[i], shark_y[i], i)

            for i in range(num_of_fires):

                # game over
                collision = collided_fire(fire_x[i], fire_y[i], car_x, car_y)
                if collision:
                    pygame.mixer.music.load('fire.mp3')
                    pygame.mixer.music.play()
                    # gameover()
                    a = 0
                    set2 = 1
                    car_x = 925
                    car_y = 990
                    # screenon = 0
                    # break
                fire(fire_x[i], fire_y[i], i)

            for i in range(num_of_fires):

                # game over
                collision = collided_fire1(fire1_x[i], fire1_y[i], car_x, car_y)
                if collision:
                    pygame.mixer.music.load('fire.mp3')
                    pygame.mixer.music.play()
                    # gameover()
                    a = 0
                    set2 = 1
                    car_x = 925
                    car_y = 990
                    # screenon = 0
                    # break
                fire1(fire1_x[i], fire1_y[i], i)

            for i in range(num_of_fires):

                # game over
                collision = collided_fire2(fire_x[i], fire2_y[i], car_x, car_y)
                if collision:
                    pygame.mixer.music.load('fire.mp3')
                    pygame.mixer.music.play()
                    # gameover()
                    a = 0
                    set2 = 1
                    car_x = 925
                    car_y = 990
                    # screenon = 0
                    # break
                fire2(fire2_x[i], fire2_y[i], i)

            if car_y > 790:
                if score_value <= 0:
                    score_value = 0
            elif 750 < car_y < 790:
                if score_value <= 10:
                    score_value = 10
            elif 640 < car_y < 750:
                if score_value <= 15:
                    score_value = 15
            elif 600 < car_y < 640:
                if score_value <= 25:
                    score_value = 25
            elif 490 < car_y < 600:
                if score_value <= 30:
                    score_value = 30
            elif 450 < car_y < 490:
                if score_value <= 35:
                    score_value = 35
            elif 340 < car_y < 450:
                if score_value <= 45:
                    score_value = 45
            elif 300 < car_y < 340:
                if score_value <= 50:
                    score_value = 50
            elif 190 < car_y < 300:
                if score_value <= 60:
                    score_value = 60
            elif 150 < car_y < 100:
                if score_value <= 65:
                    score_value = 65
            elif car_y < 150:
                if score_value <= 75:
                    score_value = 75

            show()

            if car_y < 60:
                a = 0
                set2 = 1
                car_x = 925
                car_y = 990

        if a == 0:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    screenon = False

            screen.fill((220, 20, 60))
            if k % 2 == 1:
                if set2 == 1:
                    scorer1 += score_value
                    set2 = 0
                    score_value = 0
                    time1 += second
                    second = 0
                showit(scorer1)

            if k % 2 == 0:
                if set1 == 1:
                    scorer2 += score1_value
                    set1 = 0
                    score1_value = 0
                    second = 0
                    time2 += second
                showit2(scorer2)
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    if a == 0 and k % 2 == 0:
                        counter += 1
                    a = 1
                    k += 1
                    second = 0
        if a == 1 and k % 2 == 0:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    screenon = False

            screen.fill((0, 255, 255))
            finish = font.render("Start", True, (220, 20, 60))
            screen.blit(finish, (900, 20))
            start = font.render("Finish", True, (220, 20, 60))
            screen.blit(start, (900, 960))

            pressed = pygame.key.get_pressed()
            if pressed[pygame.K_UP]:
                car1_y -= speed
            if pressed[pygame.K_DOWN]:
                car1_y += speed
            if pressed[pygame.K_LEFT]:
                car1_x -= speed
            if pressed[pygame.K_RIGHT]:
                car1_x += speed

            time_passed = clock.tick(30)
            time_passed_seconds = time_passed / 1000
            second += time_passed_seconds
            print(second)
            second1 = font.render("Time :" + str(int(second)), True, (255, 255, 255))
            screen.blit(second1, (1500, 20))
            # kepping the safe zones and start and end lines
            pygame.draw.line(screen, (220, 20, 60), (0, 60), (1910, 60), 10)
            pygame.draw.line(screen, (220, 20, 60), (0, 920), (1910, 920), 10)
            pygame.draw.line(screen, (0, 0, 255), (0, 150), (1910, 150), 40)
            pygame.draw.line(screen, (0, 0, 255), (0, 300), (1910, 300), 40)
            pygame.draw.line(screen, (0, 0, 255), (0, 450), (1910, 450), 40)
            pygame.draw.line(screen, (0, 0, 255), (0, 600), (1910, 600), 40)
            pygame.draw.line(screen, (0, 0, 255), (0, 750), (1910, 750), 40)
            # restricting the screen size
            if car1_x <= 0:
                car1_x = 1
            elif car1_x >= 1864:
                car1_x = 1863
            elif car1_y <= 1:
                car1_y = 1
            elif car1_y >= 925:
                car1_y = 925

            # boats movement
            for i in range(num_of_boats):

                # game over
                collision = collided_boat(boat_x[i], boat_y[i], car1_x, car1_y)
                if collision:
                    pygame.mixer.music.load('crash.mp3')
                    pygame.mixer.music.play()
                    # gameover()
                    a = 0
                    set1 = 1
                    car1_x = 925
                    car1_y = 20
                    # screenon = 0
                    # break
                boat_x[i] += boat_x_change[i]
                if boat_x[i] <= 0:
                    boat_x_change[i] = 1
                elif boat_x[i] >= 1864:
                    boat_x[i] = 0
                boat(boat_x[i], boat_y[i], i)

            for i in range(num_of_seas):

                # game over
                collision = collided_sea(sea_x[i], sea_y[i], car1_x, car1_y)
                if collision:
                    pygame.mixer.music.load('glass.mp3')
                    pygame.mixer.music.play()
                    # gameover()
                    a = 0
                    set1 = 1
                    car1_x = 925
                    car1_y = 20
                    # screenon = 0
                    # break
                sea_x[i] += sea_x_change[i]
                if sea_x[i] <= 0:
                    sea_x_change[i] = 1
                elif sea_x[i] >= 1864:
                    sea_x[i] = 0
                sea(sea_x[i], sea_y[i], i)

            for i in range(num_of_sharks):

                # game over
                collision = collided_shark(shark_x[i], shark_y[i], car1_x, car1_y)
                if collision:
                    pygame.mixer.music.load('shark.mp3')
                    pygame.mixer.music.play()
                    # gameover()
                    a = 0
                    set1 = 1
                    car1_x = 925
                    car1_y = 20
                    # screenon = 0
                    # break
                shark_x[i] += shark_x_change[i]
                if shark_x[i] <= 0:
                    shark_x_change[i] = 1
                elif shark_x[i] >= 1864:
                    shark_x[i] = 0
                shark(shark_x[i], shark_y[i], i)

            for i in range(num_of_fires):

                # game over
                collision = collided_fire(fire_x[i], fire_y[i], car1_x, car1_y)
                if collision:
                    pygame.mixer.music.load('fire.mp3')
                    pygame.mixer.music.play()
                    # gameover()
                    a = 0
                    set1 = 1
                    car1_x = 925
                    car1_y = 20
                    # screenon = 0
                    # break
                fire(fire_x[i], fire_y[i], i)

            for i in range(num_of_fires):

                # game over
                collision = collided_fire1(fire1_x[i], fire1_y[i], car1_x, car1_y)
                if collision:
                    pygame.mixer.music.load('fire.mp3')
                    pygame.mixer.music.play()
                    # gameover()
                    a = 0
                    set1 = 1
                    car1_x = 925
                    car1_y = 20
                    # screenon = 0
                    # break
                fire1(fire1_x[i], fire1_y[i], i)

            for i in range(num_of_fires):

                # game over
                collision = collided_fire2(fire_x[i], fire2_y[i], car1_x, car1_y)
                if collision:
                    pygame.mixer.music.load('fire.mp3')
                    pygame.mixer.music.play()
                    # gameover()
                    a = 0
                    set1 = 1
                    car1_x = 925
                    car1_y = 20
                    # screenon = 0
                    # break
                fire2(fire2_x[i], fire2_y[i], i)

            if car1_y < 150:
                if score1_value <= 0:
                    score1_value = 0
            elif 150 < car1_y < 190:
                if score1_value <= 10:
                    score1_value = 10
            elif 190 < car1_y < 300:
                if score1_value <= 15:
                    score1_value = 15
            elif 300 < car1_y < 340:
                if score1_value <= 20:
                    score1_value = 20
            elif 340 < car1_y < 450:
                if score1_value <= 30:
                    score1_value = 30
            elif 450 < car1_y < 490:
                if score1_value <= 40:
                    score1_value = 40
            elif 490 < car1_y < 600:
                if score1_value <= 45:
                    score1_value = 45
            elif 600 < car1_y < 640:
                if score1_value <= 55:
                    score1_value = 55
            elif 640 < car1_y < 750:
                if score1_value <= 60:
                    score1_value = 60
            elif 750 < car1_y < 790:
                if score1_value <= 65:
                    score1_value = 65
            elif car1_y > 790:
                if score1_value <= 75:
                    score1_value = 75
            show2()

            if car1_y > 920:
                a = 0
                set1 = 1
                car1_x = 925
                car1_y = 990

    else:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
        screen.fill((0, 119, 190))
        final1(scorer1, time1)
        final2(scorer2, time2)
        if scorer1 > scorer2:
            win1 = font.render("PLAYER1 WON", True, (255, 0, 0))
            screen.blit(win1, (400, 500))
        if scorer1 < scorer2:
            win2 = font.render("PLAYER2 WON", True, (255, 0, 0))
            screen.blit(win2, (400, 500))
        if scorer1 == scorer2:
            draw = font.render("DRAW", True, (255, 0, 0))
            screen.blit(draw, (400, 500))
        score_val2 = font.render("click F1 to exit", True, (255, 0, 0))
        screen.blit(score_val2, (400, 600))
        if event.type == pygame.KEYDOWN and event.key == pygame.K_F1:
            screenon = False
    car(car_x, car_y)
    car1(car1_x, car1_y)
    pygame.display.update()
